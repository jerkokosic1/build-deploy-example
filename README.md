# build-deploy-example



## Getting started
Steps that were done on the project:
1. Created new repository on <a href="gitlab.com">GitLab</a>
2. Created new spring project on <a href="start.spring.io">Spring initializr</a>
    - used <b>Spring Web</b> and <b>Spring Boot Actuator</b> dependencies so I can easily check if app works on endpoint GET /actuator/health
3. Created Dockerfile in root directory:
    - it is used to create docker image from jar file
    - it is made using docker image <b>openjdk:18-jdk-alpine3.15</b> which contains openjdk on alpine linux
4. Created <b>.gitlab-ci.yml</b> file which automates creating application jar file, creating a docker image out of it and saving the image in GitLab Container Repository
5. In the end minikube is used to check if the created docker image is valid and application can be created inside the container:
    - Two files for minikube are created inside the k8s folder, <b>deployment.yml</b> and <b>service.yml</b>
    - They need to be manually applied, this part is not automated.
    - After installing minikube and kubectl, next part is to apply deployment and service configurations.
    - Inside k8s folder, run commands `kubectl apply -f deployment.yml` and `kubectl apply -f service.yml`
    - To check URL where application can be accessed run command `minikube service service-name --url`
    - To check that everything works, run curl on service URL and add /actuator/health endpoint, for example `curl http://192.168.49.2:31766/actuator/health` and the result should be `{"status":"UP","groups":["liveness","readiness"]}`