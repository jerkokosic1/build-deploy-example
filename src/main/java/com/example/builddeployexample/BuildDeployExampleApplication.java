package com.example.builddeployexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildDeployExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildDeployExampleApplication.class, args);
	}

}
