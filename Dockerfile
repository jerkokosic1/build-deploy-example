FROM openjdk:18-jdk-alpine3.15
VOLUME /tmp
ADD /target/build-deploy-example-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-Djava.security.edg=file:/dev/./urandom","-jar","/app.jar"]